= kmc_tools-compact(1)

== NAME

kmc_tools-compact - remove counters of k-mers

== SYNOPSIS

*kmc_tools* compact <inputdb> [input_params] <output>

== DESCRIPTION

This command removes counters from the database and stores only k-mers.

== OPTIONS

For each input there are additional parameters:

  *-ci<value>*::
    exclude k-mers occurring less than <value> times
  *-cx<value>*::
    exclude k-mers occurring more of than <value> times

== EXAMPLE

	kmc_tools compact wy_kmc2 -ci3 -cx1000 wy_kmc1

== AUTHOR

KMC was originally written by:

 - Sebastian Deorowicz (sebastian.deorowicz@polsl.pl)

 - Marek Kokot

 - Szymon Grabowski

 - Agnieszka Debudaj-Grabysz

== COPYING

KMC is a free software distributed under GNU GPL3 licence for academic,
research, and commercial use.
