#!/bin/sh
# autopkgtest check: Build and run a program against libkmc
# Author: Sascha Steinbiss <sascha@steinbiss.name>
set -e

ORIGDIR=$(pwd)
WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM

set -v
g++ kmc_dump/*cpp -std=c++11 -lkmc -o $WORKDIR/my_kmcdump
[ -x $WORKDIR/my_kmcdump ]
echo "build: OK"

cd $WORKDIR
echo 'Running kmc (single threaded)'
kmc -ci1 -m2 -k28 -t1 $ORIGDIR/debian/tests/sample_6.fastq.gz 1 .
ls -Al
[ -s 1.kmc_suf ]
[ -s 1.kmc_pre ]
echo "kmc (single threaded): OK"

./my_kmcdump 1 out
[ -s out ]
echo "run: OK"

# Multi-threaded runs have been known to be faulty (#954270).
# Note that the thread count has been explicitly dropped from 512 to 128 due to
# hardwired limitations in newer upstream version.
echo 'Running kmc (128 threads)'
rm -f 1.kmc_suf 1.kmc_pre
kmc -ci1 -m2 -k28 -t128 $ORIGDIR/debian/tests/sample_6.fastq.gz 1 .
ls -Al
[ -s 1.kmc_suf ]
[ -s 1.kmc_pre ]
echo "kmc (128 threads): OK"

# In the end, default options should, ideally, work on any configuration.
echo 'Running kmc (default thread count)'
rm -f 1.kmc_suf 1.kmc_pre
kmc -ci1 -m2 -k28 $ORIGDIR/debian/tests/sample_6.fastq.gz 1 .
ls -Al
[ -s 1.kmc_suf ]
[ -s 1.kmc_pre ]
echo "kmc (default thread count): OK"
